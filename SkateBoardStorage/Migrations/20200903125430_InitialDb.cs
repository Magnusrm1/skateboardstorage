﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SkateBoardStorage.Migrations
{
    public partial class InitialDb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Skateboards",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DeckBrand = table.Column<string>(nullable: true),
                    DeckWidth = table.Column<string>(nullable: true),
                    DeckLength = table.Column<string>(nullable: true),
                    TruckerBrand = table.Column<string>(nullable: true),
                    TruckerWidth = table.Column<string>(nullable: true),
                    WheelBrand = table.Column<string>(nullable: true),
                    WheelDiameter = table.Column<string>(nullable: true),
                    BearingBrand = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Skateboards", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "Skateboards",
                columns: new[] { "Id", "DeckBrand", "DeckWidth", "DeckLength", "TruckerBrand", "TruckerWidth", "WheelBrand", "WheelDiameter", "BearingBrand" },
                values: new object[,]
                {
                    { 1, "Girl", "8 inch", "30 inch", "Independent", "8 inch", "Spitfire", "54mm", "Bones"},
                    { 2, "Sweet sk8brds", "8.25 inch", "30 inch", "Thunder", "8 inch", "Bones", "54mm", "Bones"},
                    { 3, "Plan B", "8.175 inch", "30 inch", "Independent", "8.25 inch", "Spitfire", "54mm", "Independent"}
                });

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Skateboards");
        }
    }
}
