﻿using AutoMapper;
using SkateBoardStorage.DTOs;
using SkateBoardStorage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SkateBoardStorage.Profiles
{
    public class SkateboardProfile : Profile
    {
        public SkateboardProfile()
        {
            CreateMap<Skateboard, SkateboardBrandDto>();
        }
    }
}
