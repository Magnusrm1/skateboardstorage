﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SkateBoardStorage.DTOs;
using SkateBoardStorage.Models;

namespace SkateBoardStorage.Controllers
{
    [Route("v1/api/skateboard")]
    [ApiController]
    public class SkateboardsController : ControllerBase
    {
        private readonly SkateboardStorageDbContext _context;
        private readonly IMapper _mapper;

        public SkateboardsController(SkateboardStorageDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: v1/api/skateboard
        [HttpGet]
        public async Task<ActionResult<IEnumerable<SkateboardBrandDto>>> GetSkateboards()
        {
            var skateboards = await _context.Skateboards.ToListAsync();

            return skateboards.Select(s => _mapper.Map<SkateboardBrandDto>(s)).ToList(); ;
        }

        // GET: v1/api/skateboard/5
        [HttpGet("{id}")]
        public async Task<ActionResult<SkateboardBrandDto>> GetSkateboard(int id)
        {
            var skateboard = await _context.Skateboards.FindAsync(id);
            
            if (skateboard == null)
            {
                return NotFound();
            }
            var skateboardDto = _mapper.Map<SkateboardBrandDto>(skateboard);
            return skateboardDto;
        }

        // PUT: v1/api/skateboard/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSkateboard(int id, Skateboard skateboard)
        {
            if (id != skateboard.Id)
            {
                return BadRequest();
            }

            _context.Entry(skateboard).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SkateboardExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: v1/api/skateboard
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Skateboard>> PostSkateboard(Skateboard skateboard)
        {
            _context.Skateboards.Add(skateboard);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetSkateboard", new { id = skateboard.Id }, skateboard);
        }

        // DELETE: v1/api/skateboard/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Skateboard>> DeleteSkateboard(int id)
        {
            var skateboard = await _context.Skateboards.FindAsync(id);
            if (skateboard == null)
            {
                return NotFound();
            }

            _context.Skateboards.Remove(skateboard);
            await _context.SaveChangesAsync();

            return skateboard;
        }

        private bool SkateboardExists(int id)
        {
            return _context.Skateboards.Any(e => e.Id == id);
        }
    }
}
