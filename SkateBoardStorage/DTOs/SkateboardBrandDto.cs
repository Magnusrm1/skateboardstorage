﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SkateBoardStorage.DTOs
{
    public class SkateboardBrandDto
    {
        public int Id { get; set; }
        public string DeckBrand { get; set; }
        public string TruckerBrand { get; set; }
        public string WheelBrand { get; set; }
        public string BearingBrand { get; set; }
    }
}
