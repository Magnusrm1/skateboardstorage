﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SkateBoardStorage.Models
{
    public class SkateboardStorageDbContext : DbContext
    {
        public DbSet<Skateboard> Skateboards { get; set; }

        public SkateboardStorageDbContext(DbContextOptions options) : base(options) { }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

        }
    }
}
