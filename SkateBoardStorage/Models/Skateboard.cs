﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SkateBoardStorage.Models
{
    public class Skateboard
    {
        public int Id { get; set; }
        public string DeckBrand { get; set; }
        public string DeckWidth { get; set; }
        public string DeckLength { get; set; }
        public string TruckerBrand { get; set; }
        public string TruckerWidth { get; set; }
        public string WheelBrand { get; set; }
        public string WheelDiameter { get; set; }
        public string BearingBrand { get; set; }
    }
}
